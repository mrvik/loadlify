# Basic `loadlify` Bug

**Describe the bug**
Short description of the bug.

**How to Reproduce**
Paste the piece of code who calls to `loadlify`.

**Expected behavior**
What should `loadlify` do.

**Logs**
If the error has a call stack paste it here (The part corresponding to `loadlify`).

**User agent (or a list of them)**
- (e.g. Chrome 67 Android)

**Loadlify version**
(e.g. Rolling, Rolling \<branch>, v3.3.1)

**Additional context**
Add any other context about the problem here.

**Some idea on how to fix or why does it happen?**
Link here the PR if you've created it.