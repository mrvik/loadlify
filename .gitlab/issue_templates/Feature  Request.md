# Loadlify feature request

**Is your feature request related to a compatibility problem? Please describe**
(E.g. I've tried to load `somescript.js` and it didn't export enything)

**Is this feature related to the `asset handlers`?**
(E.g. Add a handler for JSON files)

**Describe the solution you'd like**
A clear and concise description of what you want to happen.

**Describe alternatives you've considered**
A clear and concise description of any workarounds or features you've considered.

**Additional context**
Add any other context or screenshots about the feature request here.
