"use-strict";
//LoadlifyJS Web Loader
class loadlifyJS{
	constructor(a){
		if(typeof XMLHttpRequest!="undefined") this.prefetch();
		this.defs=a.defs||defaults.defs;
		this.deps=a.deps||defaults.deps;
		this.loaded={};
		this.loading={};
		this.flags=a.flags||defaults.flags;
		this.props=a.properties||defaults.properties;
		this.integrity=a.integrity||defaults.integrity;
		this.handlers={
			["text/css"]: async function(a, b){
				if(b.includes("astag")){
					let node=document.createElement("link");
					node.setAttribute("rel", "stylesheet");
					node.setAttribute("type", "text/css");
					node.setAttribute("data-loadlify", a.url);
					node.setAttribute("href", a.internalURL);
					document.querySelector("head").appendChild(node);
					return [a.data, {flags:b,url:a.url}];
				}
				let node=document.createElement("style");
				node.setAttribute("data-loadlify", a.url);
				node.innerHTML=a.data;
				document.querySelector("head").appendChild(node);
				return [a.data, {flags: b, url: a.url}];
			},
			["application/javascript"]: async function(a, b){
				if(b.includes("astag")){
					let node=document.createElement("script");
					node.setAttribute("data-src", a.url);
					node.innerHTML=a.data;
					document.querySelector("head").appendChild(node);
					return [document.querySelector("script[data-src='"+a.url+"']"), {flags: b,url:a.url}];
				}else{
					let c,d,e,f,g;
					c="";
					if(b.includes("requirejs")){
						let x=await this.load("requirejs", ["type: text/plain"]);
						c=x.text;
						if(typeof(this.requireConfig)=="string"){
							c+=this.requireConfig;
						}
					}
					if(b.includes("es6")){
						var exports=self.exports||{};
						
						if(b.includes("nodemodule")){
							var module={exports: {}};
						}
					}
					c+=a.data;
					d=new Function(["exports", "module"],c);
					try{
						f=d(exports, module);
					}catch(x){
						e=x;
                        //eslint-disable-next-line no-console
						console.warn("An error has ocurred on "+a.url);
					}
					g=[d, {rv:f,flags:b,url:a.url,err:e}];
					if(g[1].err){
						throw g[1].err;
					}
					if(b.includes("es6")){
						if(typeof module!="undefined"){
							exports[a.name]=module.exports;
							g[1].module=module;
						}
						g[1]["exports"]=exports;
					}
					return g;
				}
			},
			["text/html"]: async function(a, b){
				return [a.data, {flags: b, url: a.url}];
			},
			["text/plain"]: async function(a, b){
				return [a.data, {flags: b, url: a.url}];
			}
		};
	}
	get add(){
		let a={
			defs: (a, b)=>{
				return this.defs[a]=b;
			},
			deps: (a, b)=>{
				return this.deps[a]=b;
			}
		};
		return a;
	}
	prefetch(a){
		if(!a&&typeof fetch!="undefined")return this.pref=Promise.resolve();
		this.pref=new Promise(resolver=>{
			delete self.fetch;
			let x=new XMLHttpRequest();
			x.open("GET", defaults.defs.fetch, true);
			x.send();
			x.onloadend=a=>{
				new Function(a.target.response)();
				resolver(a);
			};
		});
	}
	async load(a,b=[]){
		//First stage
		if(typeof a=="undefined") return Promise.resolve(); //An empty request with a empty response :)
		if(typeof b=="string"){ //Set flags
			b=[b];
		}
		if(!b.includes("noconcatflags")){
			b=b.concat(this.flags);
		}
		if(typeof a=="object"){ //Multiple scripts to load
			let todo=[];
			a.forEach(c=>{
				todo.push(this.load(c, b));
			});
			return await Promise.all(todo);
		}
		let name=a;
		let link=this.getlink(name,b); //Get link in early load
		
		if(this.loading[link]){
			return this.loading[link];
		}
		let cache=this.caches(link,b);
		if(cache!=undefined){
			if(b.includes("reapply")){
				let proc=this.autoapply(cache,b);
				this.loading[link]=proc;
				proc.then(rt=>{
					delete this.loading[link];
					return rt;
				});
			}
			return cache;
		}
		
		let process=this.load_inner({name,link},b);
		this.loading[link]=process;
		return await process.then(rt=>{
			delete this.loading[link];
			return rt;
		}).catch(e=>{
			delete this.loading[link];
			throw e; //Rethrow error;
		});
	}
	async load_inner(a, b){
		if(!a||!a.link)return undefined;
		if(typeof a.link=="string"&&a.link.match("file://")) this.prefetch(true); //File protocol isn't supported by the Fetch API
		await this.pref;
		
		//LOAD SCRIPTS
		var loading={};
		loading.link=a.link;
		loading.name=a.name; //Sometimes they will be ==
		
		loading.request=this.buildRequest(loading.link,b);
		let depload;
		if(!b.includes("nodeps")){
			depload=this.loadeps(loading.name, b);
		}
		await fetch(loading.request).then(x=>{ //Fetch the resource
			if(x.ok){
				return x.blob().then(A=>{ //Crear un blob al principio
					loading["blob"]=A;
					if(self.URL.createObjectURL) loading["internalURL"]=URL.createObjectURL(loading.blob);
				});
			}
			throw x.statusText;
		});
		if(!b.includes("notext"))loading["text"]=await this.blobToText(loading.blob);
		loading.type=this.whatIs(loading, b); //Get the type of file.
		
		if(depload){
			loading["deps"]=await depload;
		}
		let {apply,exports}=await this.autoapply(loading,b);
		loading.apply=apply;
		loading.exports=exports;
		this.loaded[loading.link]=loading;
		return loading; //Finished \0/
	}
	async autoapply(loading,b){
		let exports={};
		let {name,blob,internalURL}=loading;
		let url=loading.link;
		let data=loading.text;
		let fn=this.handlers[loading.type].bind(this);
		let apply=await fn({name, blob, data, url, internalURL},b);
		if(apply&&apply[1]){
			exports=apply[1].exports||{};
		}
		return {apply,exports};
	}
	blobToText(a){
		return new Promise(resolver=>{
			let reader=new FileReader();
			reader.onload=()=>{
				return resolver(reader.result);
			};
			reader.readAsText(a);
		});
	}
	loadeps(a, b){
		if(this.deps.hasOwnProperty(a)){
			if(b.includes("noflagsindeps")) b=undefined;
			return this.load(this.deps[a], b);
		}
		return Promise.resolve();
	}
	getlink(d, b){
		if(d.match(/^(((http|https|file):)|(\/\/))/)) return d;
		if(this.defs.hasOwnProperty(d)) return this.defs[d];
		if(b.includes("noprefix")) return new URL(d, location);
		return new URL(this.props.prefix+d, location);
	}
	caches(a, b){
		if(b.includes("nocache")|| b.includes("force"))return undefined;
		if(this.loaded[a]!=undefined)return this.loaded[a];
		return undefined;
	}
	whatIs(a, b){
		let res=this.stripFlags(b,"type");
		if(res)return res; //If handler for requested type is not defined, it will result in a exception

		let type=a.blob.type.match(/.*\/.*/)[0]||a.blob.type;
		if(this.handlers.hasOwnProperty(type)) return type; //Exact match
        
        let handledBy; //Wildcard or partial match
        Object.keys(this.handlers).forEach(handler=>{
            if(type.match(handler))handledBy=handler;
        });
        if(handledBy)return handledBy;
        
        return "text/plain"; //Fallback handler
	}
	buildRequest(url,flags){
		let init={};
		let configurable=this.props.configurable||["method", "mode", "cache", "credentials", "redirect", "referrer", "integrity"];
		let toDelete=this.props.toDelete||["integrity"];
		configurable.forEach(prop=>{
			init[prop]=this.stripFlags(flags,prop)
		});
		if(flags.includes("nocache")||flags.includes("force"))init.cache="no-cache";
		if(!init.integrity&&this.integrity[url])init.integrity=this.integrity[url]; //Use the existing integrity object
		this.deleteFlag(flags,toDelete);
		let req=new Request(url,init);
		return req;
	}
	getFlagReg(lookfor){
		let regStr=`(${lookfor}: )(.*)`;
		let reg=new RegExp(regStr);
		return reg;
	}
	stripFlags(flags,lookfor){
		let reg=this.getFlagReg(lookfor);
		let res=flags.filter(x=>x.match(reg));
		if(res[0])return res[0].match(reg)[2];
		return undefined;
	}
	deleteFlag(flags,deleteArray){
		deleteArray.forEach(lookfor=>{
			let reg=this.getFlagReg(lookfor);
			for(let i in flags){
				if(!flags.hasOwnProperty(i))continue;
				let flag=flags[i];
				if(flag.match(reg))delete flags[i];
			}
		});
	}
	static optjQuery(){
		if(typeof jQuery == "undefined"){
			return "jquery";
		}else{
			return undefined;
		}
	}
}
let defaults={
	defs:{
		jquery: "https://unpkg.com/jquery@latest/dist/jquery.min.js",
		bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js",
		bootstrapCSS: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
		dexie: "https://unpkg.com/dexie@latest/dist/dexie.min.js",
		jqueryUI: "https://code.jquery.com/ui/1.12.1/jquery-ui.min.js",
		clipboard: "https://unpkg.com/clipboard@latest/dist/clipboard.min.js",
		handlebars: "https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.10/handlebars.min.js",
		jqueryUICSS: "https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css",
		fontAwesome: "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
		vex: "https://cdn.rawgit.com/HubSpot/vex/master/dist/js/vex.combined.min.js",
		vexCSS: "https://cdn.rawgit.com/HubSpot/vex/master/dist/css/vex.css",
		vexTheme: "https://cdn.rawgit.com/HubSpot/vex/master/dist/css/vex-theme-plain.css",
		requirejs: "https://unpkg.com/requirejs@latest/require.js",
		materializeCSS: "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css",
		materialize: "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js",
		materialIcons: "https://fonts.googleapis.com/icon?family=Material+Icons",
		["material-components-web"]: "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css",
		["material-components-web-js"]: "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js",
		["code-prettify"]: "https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js",
		socket_io: "https://unpkg.com/socket.io-client@latest/dist/socket.io.js",
		sha256: "https://unpkg.com/js-sha256@latest/build/sha256.min.js",
		AES: "https://cdn.rawgit.com/ricmoo/aes-js/master/index.js",
		lightgalleryCSS: "https://unpkg.com/lightgallery.js@latest/dist/css/lightgallery.min.css",
		["lightgallery-transitions"]: "https://unpkg.com/lightgallery.js@latest/dist/css/lg-transitions.min.css",
		lightgallery: "https://unpkg.com/lightgallery.js@latest/dist/js/lightgallery.min.js",
		["lightgallery-plugin-pager"]: "https://cdn.rawgit.com/sachinchoolur/lg-pager.js/master/dist/lg-pager.min.js",
		["lightgallery-plugin-share"]: "https://cdn.rawgit.com/sachinchoolur/lg-share.js/master/dist/lg-share.min.js",
		["lightgallery-plugin-fullscreen"]: "https://cdn.rawgit.com/sachinchoolur/lg-fullscreen.js/master/dist/lg-fullscreen.min.js",
		["lightgallery-plugin-zoom"]: "https://cdn.rawgit.com/sachinchoolur/lg-zoom.js/master/dist/lg-zoom.min.js",
		["lightgallery-plugin-video"]: "https://cdn.rawgit.com/sachinchoolur/lg-video.js/master/dist/lg-video.min.js",
		["lightgallery-plugin-thumbnail"]: "https://cdn.rawgit.com/sachinchoolur/lg-thumbnail.js/master/dist/lg-thumbnail.min.js",
		listJS: "//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js", //listJS no funciona con eval() https://github.com/javve/list.js/issues/528
		typedJS: "https://cdn.rawgit.com/mattboldt/typed.js/master/lib/typed.min.js",
		openpgp: "https://unpkg.com/openpgp@latest/dist/openpgp.min.js",
		moment: "https://unpkg.com/moment@latest/moment.js",
		zepto: "https://unpkg.com/zepto@latest/dist/zepto.min.js",
		fetch: "https://cdn.rawgit.com/github/fetch/master/fetch.js",
		vue: "https://unpkg.com/vue@latest/dist/vue.min.js",
		["vue-dev"]: "https://unpkg.com/vue@latest/dist/vue.js",
		["vue-router"]: "https://unpkg.com/vue-router@latest/dist/vue-router.js",
		["vuex"]: "https://unpkg.com/vuex@latest/dist/vuex.min.js"
	},
	deps:{
		vex: ["vexCSS", "vexTheme"],
		jqueryUI: ["jqueryUICSS", loadlifyJS.optjQuery()],
		materialize: ["materializeCSS", "materialIcons", loadlifyJS.optjQuery()],
		bootstrap: ["bootstrapCSS", loadlifyJS.optjQuery()],
		lightgallery: ["lightgalleryCSS"]
	},
	flags: [],
	properties:{
		prefix: "lib/",
		suffix: ".js"
	},
	integrity:{
		
	}
};
(function(){
	//Jump Loadlify to Global Scope
	if(!self.noExports){
		self.exports={};
	}
	self.loadlifyJS=loadlifyJS;
	self.loadlify=new loadlifyJS({});
	self.load=function(a, b){return self.loadlify.load(a, b)};
})();
